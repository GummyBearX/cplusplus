# DataTypes

---
|Primitive|Derived|User-defined|
|---| ---| ---|
|Integer (1,-1)|Function|Class|
|Float (1.11)|Array|Structure|
|Character (A,#)|Pointer|Union|
|Boolean (0,1)|Reference| Enum|

## More About Primitive Data

---

### Integer

- Each value takes 4 bytes in memory
  - SIZE = 4 bytes
  - 1 byte = 8 bits
  - one int = 32 bits in memory
- How to find which biggest integer can be stored in 32 bits?
  - For positive int `Range(unsigned) = 0 to 2^32 - 1`
  - For negative int `Range(signed) = -2^31 to 2^31 - 1`
- ~~How negative Int are sroted in the memory~~
  - ~~i have to watch again from 5:10~~

### Float

- Each value takes 4 bytes in memory
  - SIZE = 4 bytes
- Upto 7 Decimal Digits can be store
- `DOUBLE` is use to store upto 15 Decimal
  - SIZE = 8 bytes

### Character

- Each value takes 1 byte in memory
  - SIZE = 1 byte
- Each Characters ASCII value will be stored
  - `a` = `97` in ASCII

### Boolean

- Ture = 1
- Flase = 0
- SIZE = 1 byte

### [Chick Here to download Notes](https://drive.google.com/file/d/13MkCY_bHSBi3IWRFQjEfZ-IymXQxBDyF/view)
